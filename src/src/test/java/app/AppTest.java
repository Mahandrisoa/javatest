package app;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class AppTest {
    List<Double> list = (List<Double>) Arrays.asList(70.0, 40.0, 30.0, 10.0, 20.0);
    List<Double> expectedList = (List<Double>) Arrays.asList(70.0, 40.0, 30.0);

    @org.junit.Test
    public void popTwoWorstest() {
        assert(App.popTwoWorstest(list)).equals(expectedList);
    }

    @org.junit.Test
    public void mean() {
        assert(App.mean(expectedList)).equals(49.5);
    }
}