package app;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class App {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        List<Double> marks = new ArrayList<Double>();

        int n = 5;
        for(int i = 0; i < n; i++) {
            marks.add(scan.nextDouble());
        }

        // pop two last marks
        List<Double> newList = popTwoWorstest(marks);

        // Decision
        if(mean(newList) < 60.00){
            System.out.println("Echec");
        }else {
            System.out.println("Reussite");
        }
    }

    public static List<Double> popTwoWorstest(List<Double> list) {
        List<Double> newList = new ArrayList<Double>(list);
        newList.sort(Collections.reverseOrder());
        return newList.subList(0, newList.size() - 2);
    }

    public static Double mean(List<Double> list) {

        double mean = 0.0;

        for (int i = 0; i < list.size(); i++) {
            if(i == 0) {
                mean += list.get(i) * 40;
            }
            if(i == 1) {
                mean += list.get(i) * 35;
            }
            if(i == 2) {
                mean += list.get(i) * 25;
            }
        }

        mean = Math.round(mean) / 100.00;
        return mean;
    }
}
